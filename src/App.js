import Counter from "./containers/Counter/Counter";
import {Route, Switch} from "react-router-dom";
import Layout from "./components/Layout/Layout";
import ToDoList from "./containers/ToDoList/ToDoList";

const App = () => {
  return (
    <Layout>
      <Switch>
        <Route path="/" exact component={Counter}/>
        <Route path="/task-1" component={Counter}/>
        <Route path="/task-2" component={ToDoList}/>
        <Route render={() => <h1>Not Fount</h1>}/>
      </Switch>
    </Layout>
  );
};

export default App;
