import axios from 'axios';

export const INCREASE = 'INCREASE';
export const DECREASE = 'DECREASE';
export const ADD = 'ADD';
export const SUBTRACT = 'SUBTRACT';

export const FETCH_COUNTER_REQUEST = 'FETCH_COUNTER_REQUEST';
export const FETCH_COUNTER_SUCCESS = 'FETCH_COUNTER_SUCCESS';
export const FETCH_COUNTER_FAILURE = 'FETCH_COUNTER_FAILURE';

export const SAVE_COUNTER_REQUEST = 'SAVE_COUNTER_REQUEST';
export const SAVE_COUNTER_SUCCESS = 'SAVE_COUNTER_SUCCESS';
export const SAVE_COUNTER_FAILURE = 'SAVE_COUNTER_FAILURE';

export const FETCH_TASK_REQUEST = 'FETCH_TASK_REQUEST';
export const FETCH_TASK_SUCCESS = 'FETCH_TASK_SUCCESS';
export const FETCH_TASK_FAILURE = 'FETCH_TASK_FAILURE';

export const increase = () => ({type: INCREASE});
export const decrease = () => ({type: DECREASE});
export const add = value => ({type: ADD, payload: value});
export const subtract = value => ({type: SUBTRACT, payload: value});

export const fetchCounterRequest = () => ({type: FETCH_COUNTER_REQUEST});
export const fetchCounterSuccess = counter => ({type: FETCH_COUNTER_SUCCESS, payload: counter});
export const fetchCounterFailure = () => ({type: FETCH_COUNTER_FAILURE});

export const fetchCounter = () => {
  return async (dispatch) => {
    dispatch(fetchCounterRequest());

    try {
      const response = await axios.get('https://work-project-js-default-rtdb.firebaseio.com/counter.json');

      if (response.data === null) {
        dispatch(fetchCounterSuccess(0));
      } else {
        dispatch(fetchCounterSuccess(response.data));
      }
    } catch (e) {
      dispatch(fetchCounterFailure());
    }
  };
};

export const saveCounterRequest = () => ({type: SAVE_COUNTER_REQUEST});
export const saveCounterSuccess = () => ({type: SAVE_COUNTER_SUCCESS});
export const saveCounterFailure = () => ({type: SAVE_COUNTER_FAILURE});

export const saveCounter = () => {
  return async (dispatch, getState) => {
    dispatch(saveCounterRequest());

    try {
      const currentCounter = getState().counter;
      await axios.put('https://work-project-js-default-rtdb.firebaseio.com/counter.json', currentCounter);
      dispatch(saveCounterSuccess());
    } catch (e) {
      dispatch(saveCounterFailure());
    }
  };
};

export const sendTask = task => {
  return async () => {
    await axios.post('https://work-project-js-default-rtdb.firebaseio.com/tasks.json', {task});
  };
};

export const fetchTaskRequest = () => ({type: FETCH_TASK_REQUEST});
export const fetchTaskSuccess = tasks => ({type: FETCH_TASK_SUCCESS, payload: tasks});
export const fetchTaskFailure = () => ({type: FETCH_TASK_FAILURE});

export const fetchTasks = () => {
  return async (dispatch) => {
    dispatch(fetchTaskRequest());

    try {
      const response = await axios.get('https://work-project-js-default-rtdb.firebaseio.com/tasks.json');

      const tasks = Object.keys(response.data).map(id => ({
        ...response.data[id],
        id,
        checked: false,
      }));

      if (response.data === null) {
        dispatch(fetchTaskSuccess(0));
      } else {
        dispatch(fetchTaskSuccess(tasks));
      }
    } catch (e) {
      dispatch(fetchTaskFailure());
    }
  };
};

export const deleteTask = id => {
  return async () => {
    await axios.delete(`https://work-project-js-default-rtdb.firebaseio.com/tasks/${id}.json`);
  };
};
