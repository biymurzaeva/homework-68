import {
  ADD,
  DECREASE,
  INCREASE,
  SUBTRACT,
  FETCH_COUNTER_FAILURE,
  FETCH_COUNTER_REQUEST,
  FETCH_COUNTER_SUCCESS,
  SAVE_COUNTER_SUCCESS,
  SAVE_COUNTER_REQUEST,
  SAVE_COUNTER_FAILURE,
  FETCH_TASK_REQUEST,
  FETCH_TASK_SUCCESS,
  FETCH_TASK_FAILURE,
} from "./actions";

const initialState = {
  counter: 123,
  loading: false,
  error: null,
  tasks: [],
  checked: true,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case INCREASE:
      return {...state, counter: state.counter + 1};
    case DECREASE:
      return {...state, counter: state.counter - 1};
    case ADD:
      return {...state, counter: state.counter + action.payload};
    case SUBTRACT:
      return {...state, counter: state.counter - action.payload};
    case FETCH_COUNTER_REQUEST:
      return {...state, error: null, loading: true};
    case FETCH_COUNTER_SUCCESS:
      return {...state, loading: false, counter: action.payload};
    case FETCH_COUNTER_FAILURE:
      return {...state, loading: false, error: action.payload};
    case SAVE_COUNTER_REQUEST:
      return {...state, error: null, loading: true};
    case SAVE_COUNTER_SUCCESS:
        return {...state, loading: false};
    case SAVE_COUNTER_FAILURE:
        return {...state, loading: false, error: action.payload};
    case FETCH_TASK_REQUEST:
      return state;
    case FETCH_TASK_SUCCESS:
      return {...state, tasks: action.payload};
    case FETCH_TASK_FAILURE:
      return state;
    default:
      return state;
  }
};

export default reducer;