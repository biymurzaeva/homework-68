import React from 'react';
import './Task.css';

const Task = props => {
	return (
		<div className="todo">
			<div>
				{props.task}
			</div>
			<div>
				<button
					onClick={props.onDelete}
					className="remove-btn"
				>
					Delete
				</button>
			</div>
		</div>
	);
};

export default Task;