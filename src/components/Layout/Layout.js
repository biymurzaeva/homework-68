import React from 'react';
import {NavLink} from "react-router-dom";
import './Layout.css';

const Layout = ({children}) => {
	return (
		<>
			<nav className="Navs">
				<NavLink to={'/task-1'} exact className="task-1">
					Task 1
				</NavLink>
				<NavLink to={'/task-2'}>
					Task 2
				</NavLink>
			</nav>
			{children}
		</>
	);
};

export default Layout;