import React, {useEffect, useState} from 'react';
import './ToDoList.css';
import {useDispatch, useSelector} from "react-redux";
import {fetchTasks, sendTask, deleteTask} from "../../store/actions";
import Task from "../../components/Task/Task";

const ToDoList = () => {
	const dispatch = useDispatch();
	const [task, setTask] = useState('');
	const tasks = useSelector(state => state.tasks);

	useEffect(() => {
		dispatch(fetchTasks());

	}, [dispatch]);

	const addTask = async e => {
		e.preventDefault();
		await dispatch(sendTask(task));
		dispatch(fetchTasks());
	};

	const removeTask = async id => {
		await dispatch(deleteTask(id));
		dispatch(fetchTasks());
	};

	let list = [];

	if (tasks) {
		list = tasks.map(task => (
			<Task
				key={task.id}
				task={task.task}
				onDelete={() => removeTask(task.id)}
			/>
		));
	}

	return (
		<div>
			<div className="Form">
				<form onSubmit={addTask}>
					<input
						type="text"
						placeholder="Enter task name"
						value={task}
						onChange={e => setTask(e.target.value)}
						className="TaskField"
					/>
					<button type="submit" className="Btn">Add task</button>
				</form>
			</div>
			{list}
		</div>
	);
};

export default ToDoList;